package com.epam.training;

public class ConfigKeys {

    // API URLs

    public static final String API_URL = "http://localhost:8080/api";

    public static final String CREDIT_CARD_API_URL = "/creditCard";

    public static final String CREDIT_CARD_API_PAY_URL = CREDIT_CARD_API_URL + "/pay";

    public static final String PAYFRIEND_API_URL = "/payFriend";

    public static final String PAYFRIEND_API_PAY_URL = PAYFRIEND_API_URL + "/pay";

    // Credit Card remote service URLs

    public static final String CREDIT_CARD_SERVICE_URL = "http://localhost:8085";

    public static final String CREDIT_CARD_PAY_SERVICE_URL = CREDIT_CARD_SERVICE_URL + "/pay";

    // PayFriend remote service URLs

    public static final String PAYFRIEND_SERVICE_URL = "http://localhost:8087";

    public static final String PAYFRIEND_PAY_SERVICE_URL = PAYFRIEND_SERVICE_URL + "/pay";

}
