package com.epam.training.payment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class AbstractPaymentResponse {

    private double amount;
    private Integer[] paymentTime;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Integer[] getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Integer[] paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String toFormattedString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String jsonString = gson.toJson(this);

        return jsonString.replace("\n", "<br>").replace(": ", ":&nbsp;").replace(" ", "&nbsp;&nbsp;&nbsp;&nbsp;");
    }

    @Override
    public String toString() {
        Gson gson = new Gson();

        return gson.toJson(this);
    }

}
